package pe.uni.maxwelparedesl.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button buttonSiguiente, buttonAnterior, buttonVerdadero, buttonFalso;
    TextView pregunta;
    ImageView imagen;
    RelativeLayout layout;
    ArrayList<Integer> controlador= new ArrayList<>();
    int contador = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pregunta = findViewById(R.id.question);
        imagen = findViewById(R.id.image);
        buttonAnterior = findViewById(R.id.previous);
        buttonSiguiente = findViewById(R.id.next);
        buttonVerdadero = findViewById(R.id.verdadero);
        buttonFalso = findViewById(R.id.falso);
        layout = findViewById(R.id.layout);

        String[] preguntas = getResources().getStringArray(R.array.preguntas);
        Trivia t1 = new Trivia(preguntas[0],true,R.drawable.ic_launcher_background);
        Trivia t2 = new Trivia(preguntas[1],true,R.drawable.ic_launcher_foreground);
        Trivia t3 = new Trivia(preguntas[2],false,R.drawable.ic_launcher_background);
        Trivia t4 = new Trivia(preguntas[3],false,R.drawable.ic_launcher_foreground);
        Trivia t5 = new Trivia(preguntas[4],true,R.drawable.ic_launcher_background);
        Trivia t6 = new Trivia(preguntas[5],false,R.drawable.ic_launcher_foreground);

        setControlador();

        if(controlador.get(contador)==0) {

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t1.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t1.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t1.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }else if(controlador.get(contador)==1){

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t2.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t2.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t2.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }else if(controlador.get(contador)==2){

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t3.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t3.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t3.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }else if(controlador.get(contador)==3){

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t4.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t4.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t4.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }else if(controlador.get(contador)==4){

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t5.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t5.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t5.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }else if(controlador.get(contador)==5){

            imagen.setImageResource(R.drawable.ic_launcher_foreground);

            pregunta.setText(t2.getPregunta());

            buttonVerdadero.setOnClickListener(v -> {
                if (t6.getRespuesta()) Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
            });

            buttonFalso.setOnClickListener(v -> {
                if (t6.getRespuesta()) Snackbar.make(layout, "respuesta incorrecta", Snackbar.LENGTH_LONG).show();
                else Snackbar.make(layout, "respuesta correcta", Snackbar.LENGTH_LONG).show();
            });

        }

        buttonSiguiente.setOnClickListener(v-> contador = contador +1);

        buttonAnterior.setOnClickListener(v-> contador = contador -1);

    }

    public void setControlador(){
        controlador.add(0);
        controlador.add(1);
        controlador.add(2);
        controlador.add(3);
        controlador.add(4);
        controlador.add(5);
    }
}