package pe.uni.maxwelparedesl.examenfinal;

public class Trivia {
    String pregunta;
    Boolean respuesta;
    int imagen;

    public Trivia(String pregunta, Boolean respuesta, int imagen){
        this.imagen = imagen;
        this.pregunta = pregunta;
        this.respuesta = respuesta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    public int getImagen() {
        return imagen;
    }
}
